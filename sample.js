var express = require("express");
var mssql = require("mssql");
var app = express();

var config = {
        user: '',
        password: '',
        server: '',
        database: '' 
    };

app.set("view engine","jade");

app.all('/', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
 });

app.get("/",function(req,res){
	
	mssql.connect(config,function(){
		var request =new mssql.Request();
		request.query("select * from test",function(err,result){
			console.log(result);
		});
	});

	res.render("first",{name:"This is name"});
});

app.listen(8000,function(){

});